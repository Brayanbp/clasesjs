
var tabs = (function() {

	var st= {
		"elLnks": "#navbar li",
		"ctnBox": ".starter-template .alert",
		"lnkActive": "#navbar li.active"	
	};

	var dom= {};

	var flagFirstMenu = true;

	var catchDom = function(){
		dom.elLnks= $(st.elLnks);
		dom.ctnBox= $(st.ctnBox);
		dom.lnkActive= $(st.lnkActive);
	};

	var bindEvents = function(){
		dom.elLnks.on("click", events.changeMenu);
	    dom.lnkActive.trigger("click");
	};

	var events= {
		"changeMenu": function(e){
	    	var $this= $(this),
	    		target= $($this.data("menu"));
	    	if(flagFirstMenu || !$this.hasClass("active")){
	    		flagFirstMenu= false;
	    		dom.ctnBox.hide();
		    	target.show();
		    	dom.elLnks.removeClass("active");
		    	$this.addClass("active");
	    	}
	    }
	};

  var initialize = function() {
  	catchDom();
  	bindEvents();
  };

  return {
    init: initialize
  }
})();

tabs.init();