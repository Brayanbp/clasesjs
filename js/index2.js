
var tabs = (function() {

	var st= {
		"elLnks": "#navbar li",
		"lnkAbout": "#lnkAbout",
		"lnkAlert": ".lnkAlert",
		"lnkAddAlert": ".lnkAddAlert",
		"containerAlert": "#elements",
		"lnkCustomEvent": ".lnkCustomEvent"
	};

	var dom= {};

	var catchDom = function(){
		dom.elLnks= $(st.elLnks);
		dom.lnkAbout= $(st.lnkAbout);
		dom.lnkAlert= $(st.lnkAlert);
		dom.lnkAddAlert= $(st.lnkAddAlert);
		dom.containerAlert= $(st.containerAlert);
		dom.lnkCustomEvent= $(st.lnkCustomEvent);
	};

	var bindEvents = function(){
		dom.elLnks.on("click",  events.evtClick);
		dom.lnkAddAlert.on("click", events.addAlert);
		dom.lnkCustomEvent.on("customevent", events.customEvent);
	};

	var events= {
		"evtClick": function(e){
			console.log(this);
	    },
	    "evtAlert": function(e){
	    	alert("hola mundo");
	    },
	    "addAlert": function(e){
	    	var ctnLnkAlert= $("<li/>",{
		    		"class": "test"
		    	}),
	    		lnkAlert= $("<a/>",{
	    			"href": "javascript:;",
	    			"class": "lnkAlert",
	    			"text": "Alert"
	    		});
	    	ctnLnkAlert.append(lnkAlert);
	    	lnkAlert.on("click", events.evtAlert);
	    	dom.containerAlert.append(ctnLnkAlert);
	    	dom.lnkCustomEvent.trigger("customevent");
	    },
	    "customEvent": function(e){
	    	alert("custom event");
	    }
	};

	var functions= {
		"trigger": function(obj){
			events.evtClick.call(obj);
		}
	};

	var initialize = function() {
		catchDom();
		bindEvents();
		functions.trigger(dom.lnkAbout);
	};

  return {
    init: initialize
  }
})();

tabs.init();